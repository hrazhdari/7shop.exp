<?php
$fileContent = "<?php
namespace App\Repositories;
use App\Models\MODEL_CLASS;

class MODEL_CLASSRepo extends BaseRepo {
    protected \$model = MODEL_CLASS::class;

}";

$models = [
    'Payment','Shipment','User', 'Option', 'Address', 'Order', 'Product', 'Media', 'Review', 'Category', 'Attribute', 'AttributeValue',
];

foreach ($models as $modelName){
    $thisContent = str_replace('MODEL_CLASS',$modelName,$fileContent);
//    $thisContent = str_replace('TABLE_NAME',$tableName,$thisContent);
    file_put_contents("{$modelName}Repo.php",$thisContent);
}
