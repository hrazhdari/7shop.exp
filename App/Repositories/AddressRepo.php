<?php
namespace App\Repositories;
use App\Models\Address;

class AddressRepo extends BaseRepo {
    protected $model = Address::class;

}