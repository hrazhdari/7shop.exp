<?php
namespace App\Repositories;
use App\Models\Review;

class ReviewRepo extends BaseRepo {
    protected $model = Review::class;

}