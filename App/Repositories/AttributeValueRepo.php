<?php
namespace App\Repositories;
use App\Models\AttributeValue;

class AttributeValueRepo extends BaseRepo {
    protected $model = AttributeValue::class;

}