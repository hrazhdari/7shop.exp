<?php
namespace App\Controllers\Panel;

use App\Services\View\View;

class MediaController{

	public function index($request) {
		$data = [];
		View::load('panel.media.all',$data,'panel-admin');
	}
	
	public function add($request) {
		$data = [];
        View::load('panel.media.add',$data,'panel-admin');
    }

}