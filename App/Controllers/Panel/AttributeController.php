<?php
namespace App\Controllers\Panel;

use App\Services\View\View;

class AttributeController{

	public function index($request) {
		$data = [];
		View::load('panel.attribute.index',$data,'panel-admin');
	}
}