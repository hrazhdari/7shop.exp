<?php
namespace App\Services\Database;

class DbConnection{
	public $conn;

	public function __construct() {
		$dbInfo = \App\Services\Config\Config::get('database');
		$this->conn   = mysqli_connect( $dbInfo['host'], $dbInfo['dbuser'], $dbInfo['dbpass'], $dbInfo['dbname'] );
		if ( ! $this->conn ) {
			echo 'Connected failure<br>';
			die();
		}
	}

	public function query($sql) {

		return $this->conn->query($sql);
	}
}
