<?php
namespace App\Models;

class Address extends BaseModel {
	protected $table = 'addresses' ;
	protected $primaryKey = 'id' ;

    public function shipments()
    {
        return $this->hasMany(Shipment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}