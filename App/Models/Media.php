<?php
namespace App\Models;

class Media extends BaseModel {
	protected $table = 'media' ;
	protected $primaryKey = 'id' ;

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

}